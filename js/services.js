'use strict';

/* Services */

var testServices = angular.module('testServices', ['ngResource']);

testServices.factory('Test', ['$resource',
  function($resource){
    return $resource('phones/:testId', {}, {
      query: {method:'GET', params:{testId:''}, isArray:true}
    });
  }]);
