'use strict';

/* Controllers */

var testControllers = angular.module('testControllers', ['ui.bootstrap']);

testControllers.controller('TestListCtrl', ['$scope', 'Test',
    function($scope, Test) {
        $scope.Title = "TestHeading";
        $scope.subTitle = "TestSubTitle";
    }
]);

testControllers.controller('TestDetailCtrl', ['$scope', '$routeParams', 'Test',
    function($scope, $routeParams, Test) {
        
    }
]);