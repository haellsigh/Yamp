'use strict';

/* App Module */

var testApp = angular.module('testApp', [
    'ngRoute',
    'testControllers',
    'testServices'
]);

testApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/tests', {
        templateUrl: 'partials/test-list.html',
        controller: 'TestListCtrl'
      }).
      when('/tests/:testId', {
        templateUrl: 'partials/test-detail.html',
        controller: 'TestDetailCtrl'
      }).
      otherwise({
        redirectTo: '/tests'
      });
  }]);
